public class Model {


    private byte[][] spielbrett = new byte[8][8];
    private boolean spielerEins = true;
    private byte[] steinZahl = {7, 7, 7, 7, 7, 7, 7, 7};
    private byte gewinner = 0;
    private int gewinnerScorePlayer1 = 0;
    private int gewinnerScorePlayer2 = 0;
    private boolean finished = false;
    private int gewinnArt = 0;
    private byte gewinnSpalte = 0;
    private byte gewinnZeile = 0;

    public Model() {

    }

    public void spielerWechseln() {
        if (gewinner == 0) {
            if (spielerEins) {
                spielerEins = false;
            } else {
                spielerEins = true;
            }
        }
        /* spielerEins = !spielerEins */
    }

    public byte[][] getSpielbrett() {
        return spielbrett;
    }

    public void steinSetzen(int spaltenzahl) {
        if (gewinner == 0) {

            if (spielerEins) {
                if (steinZahl[spaltenzahl] >= 0) {
                    spielbrett[spaltenzahl][steinZahl[spaltenzahl]] = 1;
                    steinZahl[spaltenzahl]--;
                }
            } else {
                if (steinZahl[spaltenzahl] >= 0) {
                    spielbrett[spaltenzahl][steinZahl[spaltenzahl]] = 2;
                    steinZahl[spaltenzahl]--;
                }
            }
        }

    }

    public byte[] getSteinZahl() {
        return steinZahl;
    }

    public void update(long elapsedTime) {
        if (!finished) {
            spielLogikSenkrecht();
            spielLogikWaagrecht();
            spielLogikDiagonalRechtsNachLinks();
            spielLogikDiagonalLinksNachRechts();
            scoreBerechnen();

        }
    }

    public int getGewinnArt() {
        return gewinnArt;
    }

    public byte getGewinnSpalte() {
        return gewinnSpalte;
    }

    public byte getGewinnZeile() {
        return gewinnZeile;
    }

    public void spielLogikSenkrecht() {
        for (int s = 7; s >= 0; s--) {
            for (int z = 0; z < 5; z++) {
                if (spielbrett[s][z] == 1 && spielbrett[s][z + 1] == 1 && spielbrett[s][z + 2] == 1 && spielbrett[s][z + 3] == 1) {
                    gewinner = 1;
                    finished = true;
                    gewinnArt = 1;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
                if (spielbrett[s][z] == 2 && spielbrett[s][z + 1] == 2 && spielbrett[s][z + 2] == 2 && spielbrett[s][z + 3] == 2) {
                    gewinner = 2;
                    finished = true;
                    gewinnArt = 1;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
            }

        }
    }

    public void spielLogikWaagrecht() {
        for (int s = 4; s >= 0; s--) {
            for (int z = 7; z >= 0; z--) {
                if (spielbrett[s][z] == 1 && spielbrett[s + 1][z] == 1 && spielbrett[s + 2][z] == 1 && spielbrett[s + 3][z] == 1) {
                    gewinner = 1;
                    finished = true;
                    gewinnArt = 2;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;

                }
                if (spielbrett[s][z] == 2 && spielbrett[s + 1][z] == 2 && spielbrett[s + 2][z] == 2 && spielbrett[s + 3][z] == 2) {
                    gewinner = 2;
                    finished = true;
                    gewinnArt = 2;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
            }
        }

    }

    public void spielLogikDiagonalRechtsNachLinks() {
        for (int s = 4; s >= 0; s--) {
            for (int z = 4; z >= 0; z--) {
                if (spielbrett[s][z] == 1 && spielbrett[s + 1][z + 1] == 1 && spielbrett[s + 2][z + 2] == 1 && spielbrett[s + 3][z + 3] == 1) {
                    gewinner = 1;
                    finished = true;
                    gewinnArt = 3;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
                if (spielbrett[s][z] == 2 && spielbrett[s + 1][z + 1] == 2 && spielbrett[s + 2][z + 2] == 2 && spielbrett[s + 3][z + 3] == 2) {
                    gewinner = 2;
                    finished = true;
                    gewinnArt = 3;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
            }
        }
    }

    public void spielLogikDiagonalLinksNachRechts() {
        for (int s = 0; s <= 4; s++) {
            for (int z = 7; z >= 3; z--) {
                if (spielbrett[s][z] == 1 && spielbrett[s + 1][z - 1] == 1 && spielbrett[s + 2][z - 2] == 1 && spielbrett[s + 3][z - 3] == 1) {
                    gewinner = 1;
                    finished = true;
                    gewinnArt = 4;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
                if (spielbrett[s][z] == 2 && spielbrett[s + 1][z - 1] == 2 && spielbrett[s + 2][z - 2] == 2 && spielbrett[s + 3][z - 3] == 2) {
                    gewinner = 2;
                    finished = true;
                    gewinnArt = 4;
                    gewinnSpalte = (byte) s;
                    gewinnZeile = (byte) z;
                }
            }
        }

    }

    public byte getGewinner() {
        return gewinner;
    }

    public void scoreBerechnen() {
        if (gewinner == 1) {
            gewinnerScorePlayer1++;
        }
        if (gewinner == 2) {
            gewinnerScorePlayer2++;
        }

    }


    public void reset() {
        if (gewinner != 0) {
            gewinner = 0;
            spielbrett = new byte[8][8];
            spielerEins = true;
            finished = false;
            gewinnArt = 0;
            for (int i = 0; i < steinZahl.length; i++) {
                steinZahl[i] = 7;
            }


        }
    }


    public int getGewinnerScorePlayer1() {
        return gewinnerScorePlayer1;
    }

    public int getGewinnerScorePlayer2() {
        return gewinnerScorePlayer2;
    }
}
